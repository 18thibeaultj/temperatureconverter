/**
 * Converts temperatures between Celsius and Fahrenheit based on user input
 * 
 * @author Josh Thibeault
 * @block Blue 3, Hunt
 * @course AP Computer Science
 * 
 * Date created: 09/15/2017
 */

import java.util.Scanner;

public class TemperatureConverter {
	
	private static Scanner keyReader = new Scanner(System.in);; // Defines a Scanner object - static to protect method
	
	public static void main(String[] args) {
		System.out.println("USAGE: Enter temperature, followed by unit. Ex.: 451F");
		System.out.println("NOTE: This program requires numeric input and will not handle exceptions.");
		System.out.println(""); //Print an empty line to the terminal for cleanliness
		
		String userInput;
		String unitOfInput;
		double inputTemperature;
		double convertedTemperature = 0;
		
		while(true) {
			System.out.print("Enter temperature to convert ('exit' to exit): ");
			userInput = keyReader.nextLine();
			
			if(userInput.equalsIgnoreCase("exit")) {	//If "exit" is entered in any form, end the loop and quit
				System.exit(0);
			}
			
			unitOfInput = userInput.substring(userInput.length() - 1);	//Determine unit by parsing the last character of the string
			inputTemperature = Double.parseDouble(userInput.substring(0, userInput.length()-1)); //Determine temp value by removing last character
			
			if(unitOfInput.equalsIgnoreCase("F")) {
				convertedTemperature = convertFahrenheitToCelsius(inputTemperature);	//If in F, convert to C
				printConvertedTemp(convertedTemperature, unitOfInput);	//Written in a separate method to avoid rewriting unit detection block
			} else if(unitOfInput.equalsIgnoreCase("C")) {
				convertedTemperature = convertCelsiusToFahrenheit(inputTemperature);	//If in C, convert to F
				printConvertedTemp(convertedTemperature, unitOfInput);
			} else {
				System.out.println("Unrecognized temperature unit. Supported units are F and C.");	//If something else, give an error
			}
		}
	}
	
	private static void printConvertedTemp(double temp, String unit) {
		System.out.print("Converted temperature: " + temp);
		if(unit == "F") {
			System.out.println("C");	//Determine the unit of the converted temperature based on the input temperature
		} else {
			System.out.println("F");
		}
	}
	
	private static double convertFahrenheitToCelsius(double inputTemperature) {
		return ((inputTemperature - 32) / (9/5));	//Return value in cel
	}
	
	private static double convertCelsiusToFahrenheit(double inputTemperature) {
		return (inputTemperature * (9/5) + 32);	//Return value in fah
	}

}
